Title: Eclipseのエディタの背景色と文字色を変えるプラグイン
Slug: how-to-use-eclipse-color-theme
URL: archives/277
save_as: archives/277/index.html
Author: higebu
Category: Tech
Tags: eclipse, color, theme

白地だと眩しいから色を変えよう
↓
設定が面倒
↓
プラグインあった

という流れ

忘れないようにメモしておく

[Eclipse Color Theme](http://marketplace.eclipse.org/content/eclipse-color-theme)というやつです

リンク先から落としてもいいし、マーケットプレイスからインストールすることもできる

初めから20個くらいテーマが入ってるけど、他の人が作ったテーマを探せるところがある

http://www.eclipsecolorthemes.org/

オリジナルのテーマをアップすることができて、現在1715テーマもある（2011年5月17日時点）

設定画面はこんな感じ↓

{% img /images/2011/05/eclipse-color-theme1.png 640 489 "Eclipse Color Theme" "Eclipse Color Theme" %}
