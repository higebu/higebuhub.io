Title: Zabbix Conference Japan 2013でVMware監視機能について発表してきました
Slug: zabbix-conference-japan-2013
Date: 2013-11-23 19:44
Category: Tech
Tags: zabbix, vmware, conference
Summary: 11/22に開催された[Zabbix Conference Japan 2013](http://www.zabbix.com/jp/conference_japan_2013.php)でVMware監視機能について発表してきました。

11/22に開催された[Zabbix Conference Japan 2013](http://www.zabbix.com/jp/conference_japan_2013.php)でVMware監視機能について発表してきました。

発表資料

<iframe src="http://www.slideshare.net/slideshow/embed_code/28568152" width="597" height="486" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC;border-width:1px 1px 0;margin-bottom:5px" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="https://www.slideshare.net/higebu/zabbix-conference-japan-2013vmwaremonitoring" title="Zabbix Conference Japan 2013 VMware monitoring" target="_blank">Zabbix Conference Japan 2013 VMware monitoring</a> </strong> from <strong><a href="http://www.slideshare.net/higebu" target="_blank">Yuya Kusakabe</a></strong> </div>

11/12に2.2.0がリリースされてから1週間後の11/19に本番導入しています。

検証はα版の頃からやっていますが、結構大変でした。


他の監視もZabbixに統合しようと思っているので、また何か出せる情報があれば、このブログやどこかの勉強会で共有していきたいと思っています。
