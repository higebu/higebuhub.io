Title: ロードバイク買いました
Slug: i-boutht-a-road-bike
URL: archives/312
save_as: archives/312/index.html
Author: higebu
Category: Road bike
Tags: road bike

{% img /images/2011/10/20111010-123442.jpg 560 418 "GIOS Al Lite" "GIOS Al Lite" %}

8月28日に買いました。

本体は越谷レイクタウンのイオンの中に入っているワイズロードで96,600円。

もろもろ合わせて12万くらいかな。

スペックはこちら→<http://www.chari-u.com/gios07/7allite07gios.htm>

定価は120,750円なのですが、2012年モデルが出てきたので安くなりました。

買ってから今までで160kmくらい走ったようです。→[RunKeeper FitnessReports][1]

よく行く美容院の店長さんに言われて名前まで付けましたｗ

とりあえずはその美容院までこれに乗って行くのが目標です。

片道45kmくらいだけどアップダウンが激しいからきつそう。

以下、iPhone4を載せられるようにした話。

買ったものは以下の3点。

[MSY 自転車用マウンタ iCrew 4 for iPhone 3G/3GS/4(広角レンズ付) ブラック MS-IC02CABK][2]
[キャットアイ(CAT EYE) CC-CD200N/CC-CD100N用アタッチメントキット #169-9757N][3]
[INFINI アークスパン CG-901][4]

アークスパン CG-901だけ現在Amazonにありませんでした。

自分は会社の近くの自転車屋で買いました。

↓取り付けるとこんな感じ

{% img /images/2011/10/20111010-123511.jpg 560 418 "iCrew 4 for iPhone" "iCrew 4 for iPhone" %}

↓iPhone入れるやつ外すとこんな感じ

{% img /images/2011/10/20111010-123520.jpg 560 418 "No iCrew 4 for iPhone" "No iCrew 4 for iPhone" %}

回して画面の角度を調節できるんです。本体回しても画面が回らないのであまり使わなそうですがｗ

iCrew4は、**取り付け可能なハンドルバーサイズは22.2mm~23.8mmのJIS規格サイズのみ**

ということで、GIOSの38mmくらいあるハンドルには絶対に取り付けられない感じでした。

そこでアークスパン CG-901が出てくるんですが、これもハンドル径的に無理でした。

しかし、アークスパン CG-901はネジ穴が貫通しているので、写真のようにCATEYEの結束バンドでとめることができました。

まだ画面の角度が微妙なので後で調整します。

では、また何か取り付けたら報告します。

 [1]: http://runkeeper.com/user/higebu/fitnessReports
 [2]: http://www.amazon.co.jp/gp/product/B004KZQOB0/ref=as_li_ss_tl?ie=UTF8&tag=higebu-22&linkCode=as2&camp=247&creative=7399&creativeASIN=B004KZQOB0
 [3]: http://www.amazon.co.jp/gp/product/B001IKK2AO/ref=as_li_ss_tl?ie=UTF8&tag=higebu-22&linkCode=as2&camp=247&creative=7399&creativeASIN=B001IKK2AO
 [4]: http://item.rakuten.co.jp/qbei/2infni_017191/
